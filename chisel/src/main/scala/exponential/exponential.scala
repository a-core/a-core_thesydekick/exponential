
// Chisel module exponential
// This is a Chisel module for CORDIC exponential function accelerator.
// Inititally written by chisel-blocks-utils initmodule.sh, 2023-04-09
package exponential

import chisel3._
import chisel3.util._
import chisel3.experimental._
import chisel3.stage.{ChiselStage, ChiselGeneratorAnnotation}

/** IO definitions for exponential */
class exponentialIO
   extends Bundle {
        val A       = Input(UInt(32.W))
        val Z       = Output(UInt(32.W))
}

class exponential extends Module {
/** Defining IOs for the module */ 
    val io       = IO(new exponentialIO)

/** Lookup table for the k values and bit shifts */
/** 4 bit for whole numbers 28 bit for decimals */
/** LUT value calculation    LUT = (bit_shift)*ln(2) */
/** Example for 8 bitshift:  LUT = 8*ln(2) = 5.545177 */
/** 4 bit for whole numbers: LUT = 5.545177 << 28 = 1488522235 */
/** If you change input vector remember to update testbench */


        val rom           = VecInit(1860652794.U,
                                    1488522235.U,
                                    1116391676.U,
                                    744261117.U,
                                    372130558.U,
                                    186065279.U,
                                    108841211.U,
                                    59899640.U,
                                    31617142.U, 
                                    16273798.U,
                                    8260204.U,
                                    4161873.U,
                                    2089002.U,
                                    1046533.U,
                                    523776.U,
                                    262016.U)

        val bit_shift_lut = VecInit(10.U, 8.U, 6.U, 4.U, 2.U, 1.U, 1.U, 2.U, 3.U, 4.U, 5.U, 6.U, 7.U, 8.U, 9.U, 10.U)
            
        val register        = RegInit(VecInit(Seq.fill(17)(0.U(32.W))))
        val result_register = RegInit(VecInit(Seq.fill(17)(0.U(32.W))))

        register(0)             := io.A
        /** In result register 8 bits for decimals and 24 bits for whole numbers. */
        /** If you change output vector, remember to update sydekick testbench. */
        result_register(0)      := (1.U << 8)
        
        /** Iterate through the lookup table */
        for (i <- 0 to 15) {
            when (register(i) > rom(i)) {
                if(i < 6) {
                    register(i + 1)        := register(i) - rom(i)
                    result_register(i + 1) := result_register(i) << bit_shift_lut(i)
                } else {
                    register(i + 1)        := register(i) - rom(i)
                    result_register(i + 1) := result_register(i) + (result_register(i) >> bit_shift_lut(i))
                    }
            } .otherwise {
                register(i + 1)         := register(i)
                result_register(i + 1)  := result_register(i)
            }
            }
        /** Realtive error is constant, so this is for correcting the relative error */
        io.Z := result_register(16) + (result_register(16) >> 7) 
}

/** This gives you verilog10 */
object exponential extends App {
    val annos = Seq(ChiselGeneratorAnnotation(() => new exponential))
    (new ChiselStage).execute(args, annos)
}

