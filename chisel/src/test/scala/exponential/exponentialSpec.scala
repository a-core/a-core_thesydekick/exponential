package exponential

import chisel3._
import chiseltest._
import org.scalatest.flatspec.AnyFlatSpec
import dsptools.{DspTester, DspTesterOptionsManager, DspTesterOptions}
import dsptools.numbers._

class exponentialSpec extends AnyFlatSpec with ChiselScalatestTester {

  it should "propagate values in the register" in {
    test(new exponential) { dut =>
      dut.io.A.poke(196608)
      dut.clock.step(6)
      dut.io.Z.expect(1310720)
    }
  }
}
