module exponential(
  input         clock,
  input         reset,
  input  [31:0] io_A,
  output [31:0] io_Z
);
`ifdef RANDOMIZE_REG_INIT
  reg [31:0] _RAND_0;
  reg [31:0] _RAND_1;
  reg [31:0] _RAND_2;
  reg [31:0] _RAND_3;
  reg [31:0] _RAND_4;
  reg [31:0] _RAND_5;
  reg [31:0] _RAND_6;
  reg [31:0] _RAND_7;
  reg [31:0] _RAND_8;
  reg [31:0] _RAND_9;
  reg [31:0] _RAND_10;
  reg [31:0] _RAND_11;
  reg [31:0] _RAND_12;
  reg [31:0] _RAND_13;
  reg [31:0] _RAND_14;
  reg [31:0] _RAND_15;
  reg [31:0] _RAND_16;
  reg [31:0] _RAND_17;
  reg [31:0] _RAND_18;
  reg [31:0] _RAND_19;
  reg [31:0] _RAND_20;
  reg [31:0] _RAND_21;
  reg [31:0] _RAND_22;
  reg [31:0] _RAND_23;
  reg [31:0] _RAND_24;
  reg [31:0] _RAND_25;
  reg [31:0] _RAND_26;
  reg [31:0] _RAND_27;
  reg [31:0] _RAND_28;
  reg [31:0] _RAND_29;
  reg [31:0] _RAND_30;
  reg [31:0] _RAND_31;
  reg [31:0] _RAND_32;
`endif // RANDOMIZE_REG_INIT
  reg [31:0] register_0; // @[exponential.scala 50:38]
  reg [31:0] register_1; // @[exponential.scala 50:38]
  reg [31:0] register_2; // @[exponential.scala 50:38]
  reg [31:0] register_3; // @[exponential.scala 50:38]
  reg [31:0] register_4; // @[exponential.scala 50:38]
  reg [31:0] register_5; // @[exponential.scala 50:38]
  reg [31:0] register_6; // @[exponential.scala 50:38]
  reg [31:0] register_7; // @[exponential.scala 50:38]
  reg [31:0] register_8; // @[exponential.scala 50:38]
  reg [31:0] register_9; // @[exponential.scala 50:38]
  reg [31:0] register_10; // @[exponential.scala 50:38]
  reg [31:0] register_11; // @[exponential.scala 50:38]
  reg [31:0] register_12; // @[exponential.scala 50:38]
  reg [31:0] register_13; // @[exponential.scala 50:38]
  reg [31:0] register_14; // @[exponential.scala 50:38]
  reg [31:0] register_15; // @[exponential.scala 50:38]
  reg [31:0] result_register_0; // @[exponential.scala 51:38]
  reg [31:0] result_register_1; // @[exponential.scala 51:38]
  reg [31:0] result_register_2; // @[exponential.scala 51:38]
  reg [31:0] result_register_3; // @[exponential.scala 51:38]
  reg [31:0] result_register_4; // @[exponential.scala 51:38]
  reg [31:0] result_register_5; // @[exponential.scala 51:38]
  reg [31:0] result_register_6; // @[exponential.scala 51:38]
  reg [31:0] result_register_7; // @[exponential.scala 51:38]
  reg [31:0] result_register_8; // @[exponential.scala 51:38]
  reg [31:0] result_register_9; // @[exponential.scala 51:38]
  reg [31:0] result_register_10; // @[exponential.scala 51:38]
  reg [31:0] result_register_11; // @[exponential.scala 51:38]
  reg [31:0] result_register_12; // @[exponential.scala 51:38]
  reg [31:0] result_register_13; // @[exponential.scala 51:38]
  reg [31:0] result_register_14; // @[exponential.scala 51:38]
  reg [31:0] result_register_15; // @[exponential.scala 51:38]
  reg [31:0] result_register_16; // @[exponential.scala 51:38]
  wire [31:0] _register_1_T_1 = register_0 - 32'h6ee74efa; // @[exponential.scala 62:59]
  wire [41:0] _GEN_32 = {result_register_0, 10'h0}; // @[exponential.scala 63:66]
  wire [46:0] _result_register_1_T = {{5'd0}, _GEN_32}; // @[exponential.scala 63:66]
  wire [46:0] _GEN_1 = register_0 > 32'h6ee74efa ? _result_register_1_T : {{15'd0}, result_register_0}; // @[exponential.scala 60:41 63:44 70:41]
  wire [31:0] _register_2_T_1 = register_1 - 32'h58b90bfb; // @[exponential.scala 62:59]
  wire [39:0] _GEN_33 = {result_register_1, 8'h0}; // @[exponential.scala 63:66]
  wire [46:0] _result_register_2_T = {{7'd0}, _GEN_33}; // @[exponential.scala 63:66]
  wire [46:0] _GEN_3 = register_1 > 32'h58b90bfb ? _result_register_2_T : {{15'd0}, result_register_1}; // @[exponential.scala 60:41 63:44 70:41]
  wire [31:0] _register_3_T_1 = register_2 - 32'h428ac8fc; // @[exponential.scala 62:59]
  wire [37:0] _GEN_34 = {result_register_2, 6'h0}; // @[exponential.scala 63:66]
  wire [46:0] _result_register_3_T = {{9'd0}, _GEN_34}; // @[exponential.scala 63:66]
  wire [46:0] _GEN_5 = register_2 > 32'h428ac8fc ? _result_register_3_T : {{15'd0}, result_register_2}; // @[exponential.scala 60:41 63:44 70:41]
  wire [31:0] _register_4_T_1 = register_3 - 32'h2c5c85fd; // @[exponential.scala 62:59]
  wire [35:0] _GEN_35 = {result_register_3, 4'h0}; // @[exponential.scala 63:66]
  wire [46:0] _result_register_4_T = {{11'd0}, _GEN_35}; // @[exponential.scala 63:66]
  wire [46:0] _GEN_7 = register_3 > 32'h2c5c85fd ? _result_register_4_T : {{15'd0}, result_register_3}; // @[exponential.scala 60:41 63:44 70:41]
  wire [31:0] _register_5_T_1 = register_4 - 32'h162e42fe; // @[exponential.scala 62:59]
  wire [33:0] _GEN_36 = {result_register_4, 2'h0}; // @[exponential.scala 63:66]
  wire [46:0] _result_register_5_T = {{13'd0}, _GEN_36}; // @[exponential.scala 63:66]
  wire [46:0] _GEN_9 = register_4 > 32'h162e42fe ? _result_register_5_T : {{15'd0}, result_register_4}; // @[exponential.scala 60:41 63:44 70:41]
  wire [31:0] _register_6_T_1 = register_5 - 32'hb17217f; // @[exponential.scala 62:59]
  wire [32:0] _GEN_37 = {result_register_5, 1'h0}; // @[exponential.scala 63:66]
  wire [46:0] _result_register_6_T = {{14'd0}, _GEN_37}; // @[exponential.scala 63:66]
  wire [46:0] _GEN_11 = register_5 > 32'hb17217f ? _result_register_6_T : {{15'd0}, result_register_5}; // @[exponential.scala 60:41 63:44 70:41]
  wire [31:0] _register_7_T_1 = register_6 - 32'h67cc8fb; // @[exponential.scala 65:59]
  wire [31:0] _result_register_7_T = {{1'd0}, result_register_6[31:1]}; // @[exponential.scala 66:88]
  wire [31:0] _result_register_7_T_2 = result_register_6 + _result_register_7_T; // @[exponential.scala 66:66]
  wire [31:0] _register_8_T_1 = register_7 - 32'h391fef8; // @[exponential.scala 65:59]
  wire [31:0] _result_register_8_T = {{2'd0}, result_register_7[31:2]}; // @[exponential.scala 66:88]
  wire [31:0] _result_register_8_T_2 = result_register_7 + _result_register_8_T; // @[exponential.scala 66:66]
  wire [31:0] _register_9_T_1 = register_8 - 32'h1e27076; // @[exponential.scala 65:59]
  wire [31:0] _result_register_9_T = {{3'd0}, result_register_8[31:3]}; // @[exponential.scala 66:88]
  wire [31:0] _result_register_9_T_2 = result_register_8 + _result_register_9_T; // @[exponential.scala 66:66]
  wire [31:0] _register_10_T_1 = register_9 - 32'hf85186; // @[exponential.scala 65:59]
  wire [31:0] _result_register_10_T = {{4'd0}, result_register_9[31:4]}; // @[exponential.scala 66:88]
  wire [31:0] _result_register_10_T_2 = result_register_9 + _result_register_10_T; // @[exponential.scala 66:66]
  wire [31:0] _register_11_T_1 = register_10 - 32'h7e0a6c; // @[exponential.scala 65:59]
  wire [31:0] _result_register_11_T = {{5'd0}, result_register_10[31:5]}; // @[exponential.scala 66:88]
  wire [31:0] _result_register_11_T_2 = result_register_10 + _result_register_11_T; // @[exponential.scala 66:66]
  wire [31:0] _register_12_T_1 = register_11 - 32'h3f8151; // @[exponential.scala 65:59]
  wire [31:0] _result_register_12_T = {{6'd0}, result_register_11[31:6]}; // @[exponential.scala 66:88]
  wire [31:0] _result_register_12_T_2 = result_register_11 + _result_register_12_T; // @[exponential.scala 66:66]
  wire [31:0] _register_13_T_1 = register_12 - 32'h1fe02a; // @[exponential.scala 65:59]
  wire [31:0] _result_register_13_T = {{7'd0}, result_register_12[31:7]}; // @[exponential.scala 66:88]
  wire [31:0] _result_register_13_T_2 = result_register_12 + _result_register_13_T; // @[exponential.scala 66:66]
  wire [31:0] _register_14_T_1 = register_13 - 32'hff805; // @[exponential.scala 65:59]
  wire [31:0] _result_register_14_T = {{8'd0}, result_register_13[31:8]}; // @[exponential.scala 66:88]
  wire [31:0] _result_register_14_T_2 = result_register_13 + _result_register_14_T; // @[exponential.scala 66:66]
  wire [31:0] _register_15_T_1 = register_14 - 32'h7fe00; // @[exponential.scala 65:59]
  wire [31:0] _result_register_15_T = {{9'd0}, result_register_14[31:9]}; // @[exponential.scala 66:88]
  wire [31:0] _result_register_15_T_2 = result_register_14 + _result_register_15_T; // @[exponential.scala 66:66]
  wire [31:0] _result_register_16_T = {{10'd0}, result_register_15[31:10]}; // @[exponential.scala 66:88]
  wire [31:0] _result_register_16_T_2 = result_register_15 + _result_register_16_T; // @[exponential.scala 66:66]
  wire [31:0] _GEN_48 = {{7'd0}, result_register_16[31:7]}; // @[exponential.scala 74:37]
  wire [46:0] _GEN_49 = reset ? 47'h0 : _GEN_1; // @[exponential.scala 51:{38,38}]
  wire [46:0] _GEN_50 = reset ? 47'h0 : _GEN_3; // @[exponential.scala 51:{38,38}]
  wire [46:0] _GEN_51 = reset ? 47'h0 : _GEN_5; // @[exponential.scala 51:{38,38}]
  wire [46:0] _GEN_52 = reset ? 47'h0 : _GEN_7; // @[exponential.scala 51:{38,38}]
  wire [46:0] _GEN_53 = reset ? 47'h0 : _GEN_9; // @[exponential.scala 51:{38,38}]
  wire [46:0] _GEN_54 = reset ? 47'h0 : _GEN_11; // @[exponential.scala 51:{38,38}]
  assign io_Z = result_register_16 + _GEN_48; // @[exponential.scala 74:37]
  always @(posedge clock) begin
    if (reset) begin // @[exponential.scala 50:38]
      register_0 <= 32'h0; // @[exponential.scala 50:38]
    end else begin
      register_0 <= io_A; // @[exponential.scala 53:33]
    end
    if (reset) begin // @[exponential.scala 50:38]
      register_1 <= 32'h0; // @[exponential.scala 50:38]
    end else if (register_0 > 32'h6ee74efa) begin // @[exponential.scala 60:41]
      register_1 <= _register_1_T_1; // @[exponential.scala 62:44]
    end else begin
      register_1 <= register_0; // @[exponential.scala 69:41]
    end
    if (reset) begin // @[exponential.scala 50:38]
      register_2 <= 32'h0; // @[exponential.scala 50:38]
    end else if (register_1 > 32'h58b90bfb) begin // @[exponential.scala 60:41]
      register_2 <= _register_2_T_1; // @[exponential.scala 62:44]
    end else begin
      register_2 <= register_1; // @[exponential.scala 69:41]
    end
    if (reset) begin // @[exponential.scala 50:38]
      register_3 <= 32'h0; // @[exponential.scala 50:38]
    end else if (register_2 > 32'h428ac8fc) begin // @[exponential.scala 60:41]
      register_3 <= _register_3_T_1; // @[exponential.scala 62:44]
    end else begin
      register_3 <= register_2; // @[exponential.scala 69:41]
    end
    if (reset) begin // @[exponential.scala 50:38]
      register_4 <= 32'h0; // @[exponential.scala 50:38]
    end else if (register_3 > 32'h2c5c85fd) begin // @[exponential.scala 60:41]
      register_4 <= _register_4_T_1; // @[exponential.scala 62:44]
    end else begin
      register_4 <= register_3; // @[exponential.scala 69:41]
    end
    if (reset) begin // @[exponential.scala 50:38]
      register_5 <= 32'h0; // @[exponential.scala 50:38]
    end else if (register_4 > 32'h162e42fe) begin // @[exponential.scala 60:41]
      register_5 <= _register_5_T_1; // @[exponential.scala 62:44]
    end else begin
      register_5 <= register_4; // @[exponential.scala 69:41]
    end
    if (reset) begin // @[exponential.scala 50:38]
      register_6 <= 32'h0; // @[exponential.scala 50:38]
    end else if (register_5 > 32'hb17217f) begin // @[exponential.scala 60:41]
      register_6 <= _register_6_T_1; // @[exponential.scala 62:44]
    end else begin
      register_6 <= register_5; // @[exponential.scala 69:41]
    end
    if (reset) begin // @[exponential.scala 50:38]
      register_7 <= 32'h0; // @[exponential.scala 50:38]
    end else if (register_6 > 32'h67cc8fb) begin // @[exponential.scala 60:41]
      register_7 <= _register_7_T_1; // @[exponential.scala 65:44]
    end else begin
      register_7 <= register_6; // @[exponential.scala 69:41]
    end
    if (reset) begin // @[exponential.scala 50:38]
      register_8 <= 32'h0; // @[exponential.scala 50:38]
    end else if (register_7 > 32'h391fef8) begin // @[exponential.scala 60:41]
      register_8 <= _register_8_T_1; // @[exponential.scala 65:44]
    end else begin
      register_8 <= register_7; // @[exponential.scala 69:41]
    end
    if (reset) begin // @[exponential.scala 50:38]
      register_9 <= 32'h0; // @[exponential.scala 50:38]
    end else if (register_8 > 32'h1e27076) begin // @[exponential.scala 60:41]
      register_9 <= _register_9_T_1; // @[exponential.scala 65:44]
    end else begin
      register_9 <= register_8; // @[exponential.scala 69:41]
    end
    if (reset) begin // @[exponential.scala 50:38]
      register_10 <= 32'h0; // @[exponential.scala 50:38]
    end else if (register_9 > 32'hf85186) begin // @[exponential.scala 60:41]
      register_10 <= _register_10_T_1; // @[exponential.scala 65:44]
    end else begin
      register_10 <= register_9; // @[exponential.scala 69:41]
    end
    if (reset) begin // @[exponential.scala 50:38]
      register_11 <= 32'h0; // @[exponential.scala 50:38]
    end else if (register_10 > 32'h7e0a6c) begin // @[exponential.scala 60:41]
      register_11 <= _register_11_T_1; // @[exponential.scala 65:44]
    end else begin
      register_11 <= register_10; // @[exponential.scala 69:41]
    end
    if (reset) begin // @[exponential.scala 50:38]
      register_12 <= 32'h0; // @[exponential.scala 50:38]
    end else if (register_11 > 32'h3f8151) begin // @[exponential.scala 60:41]
      register_12 <= _register_12_T_1; // @[exponential.scala 65:44]
    end else begin
      register_12 <= register_11; // @[exponential.scala 69:41]
    end
    if (reset) begin // @[exponential.scala 50:38]
      register_13 <= 32'h0; // @[exponential.scala 50:38]
    end else if (register_12 > 32'h1fe02a) begin // @[exponential.scala 60:41]
      register_13 <= _register_13_T_1; // @[exponential.scala 65:44]
    end else begin
      register_13 <= register_12; // @[exponential.scala 69:41]
    end
    if (reset) begin // @[exponential.scala 50:38]
      register_14 <= 32'h0; // @[exponential.scala 50:38]
    end else if (register_13 > 32'hff805) begin // @[exponential.scala 60:41]
      register_14 <= _register_14_T_1; // @[exponential.scala 65:44]
    end else begin
      register_14 <= register_13; // @[exponential.scala 69:41]
    end
    if (reset) begin // @[exponential.scala 50:38]
      register_15 <= 32'h0; // @[exponential.scala 50:38]
    end else if (register_14 > 32'h7fe00) begin // @[exponential.scala 60:41]
      register_15 <= _register_15_T_1; // @[exponential.scala 65:44]
    end else begin
      register_15 <= register_14; // @[exponential.scala 69:41]
    end
    if (reset) begin // @[exponential.scala 51:38]
      result_register_0 <= 32'h0; // @[exponential.scala 51:38]
    end else begin
      result_register_0 <= 32'h100; // @[exponential.scala 56:33]
    end
    result_register_1 <= _GEN_49[31:0]; // @[exponential.scala 51:{38,38}]
    result_register_2 <= _GEN_50[31:0]; // @[exponential.scala 51:{38,38}]
    result_register_3 <= _GEN_51[31:0]; // @[exponential.scala 51:{38,38}]
    result_register_4 <= _GEN_52[31:0]; // @[exponential.scala 51:{38,38}]
    result_register_5 <= _GEN_53[31:0]; // @[exponential.scala 51:{38,38}]
    result_register_6 <= _GEN_54[31:0]; // @[exponential.scala 51:{38,38}]
    if (reset) begin // @[exponential.scala 51:38]
      result_register_7 <= 32'h0; // @[exponential.scala 51:38]
    end else if (register_6 > 32'h67cc8fb) begin // @[exponential.scala 60:41]
      result_register_7 <= _result_register_7_T_2; // @[exponential.scala 66:44]
    end else begin
      result_register_7 <= result_register_6; // @[exponential.scala 70:41]
    end
    if (reset) begin // @[exponential.scala 51:38]
      result_register_8 <= 32'h0; // @[exponential.scala 51:38]
    end else if (register_7 > 32'h391fef8) begin // @[exponential.scala 60:41]
      result_register_8 <= _result_register_8_T_2; // @[exponential.scala 66:44]
    end else begin
      result_register_8 <= result_register_7; // @[exponential.scala 70:41]
    end
    if (reset) begin // @[exponential.scala 51:38]
      result_register_9 <= 32'h0; // @[exponential.scala 51:38]
    end else if (register_8 > 32'h1e27076) begin // @[exponential.scala 60:41]
      result_register_9 <= _result_register_9_T_2; // @[exponential.scala 66:44]
    end else begin
      result_register_9 <= result_register_8; // @[exponential.scala 70:41]
    end
    if (reset) begin // @[exponential.scala 51:38]
      result_register_10 <= 32'h0; // @[exponential.scala 51:38]
    end else if (register_9 > 32'hf85186) begin // @[exponential.scala 60:41]
      result_register_10 <= _result_register_10_T_2; // @[exponential.scala 66:44]
    end else begin
      result_register_10 <= result_register_9; // @[exponential.scala 70:41]
    end
    if (reset) begin // @[exponential.scala 51:38]
      result_register_11 <= 32'h0; // @[exponential.scala 51:38]
    end else if (register_10 > 32'h7e0a6c) begin // @[exponential.scala 60:41]
      result_register_11 <= _result_register_11_T_2; // @[exponential.scala 66:44]
    end else begin
      result_register_11 <= result_register_10; // @[exponential.scala 70:41]
    end
    if (reset) begin // @[exponential.scala 51:38]
      result_register_12 <= 32'h0; // @[exponential.scala 51:38]
    end else if (register_11 > 32'h3f8151) begin // @[exponential.scala 60:41]
      result_register_12 <= _result_register_12_T_2; // @[exponential.scala 66:44]
    end else begin
      result_register_12 <= result_register_11; // @[exponential.scala 70:41]
    end
    if (reset) begin // @[exponential.scala 51:38]
      result_register_13 <= 32'h0; // @[exponential.scala 51:38]
    end else if (register_12 > 32'h1fe02a) begin // @[exponential.scala 60:41]
      result_register_13 <= _result_register_13_T_2; // @[exponential.scala 66:44]
    end else begin
      result_register_13 <= result_register_12; // @[exponential.scala 70:41]
    end
    if (reset) begin // @[exponential.scala 51:38]
      result_register_14 <= 32'h0; // @[exponential.scala 51:38]
    end else if (register_13 > 32'hff805) begin // @[exponential.scala 60:41]
      result_register_14 <= _result_register_14_T_2; // @[exponential.scala 66:44]
    end else begin
      result_register_14 <= result_register_13; // @[exponential.scala 70:41]
    end
    if (reset) begin // @[exponential.scala 51:38]
      result_register_15 <= 32'h0; // @[exponential.scala 51:38]
    end else if (register_14 > 32'h7fe00) begin // @[exponential.scala 60:41]
      result_register_15 <= _result_register_15_T_2; // @[exponential.scala 66:44]
    end else begin
      result_register_15 <= result_register_14; // @[exponential.scala 70:41]
    end
    if (reset) begin // @[exponential.scala 51:38]
      result_register_16 <= 32'h0; // @[exponential.scala 51:38]
    end else if (register_15 > 32'h3ff80) begin // @[exponential.scala 60:41]
      result_register_16 <= _result_register_16_T_2; // @[exponential.scala 66:44]
    end else begin
      result_register_16 <= result_register_15; // @[exponential.scala 70:41]
    end
  end
// Register and memory initialization
`ifdef RANDOMIZE_GARBAGE_ASSIGN
`define RANDOMIZE
`endif
`ifdef RANDOMIZE_INVALID_ASSIGN
`define RANDOMIZE
`endif
`ifdef RANDOMIZE_REG_INIT
`define RANDOMIZE
`endif
`ifdef RANDOMIZE_MEM_INIT
`define RANDOMIZE
`endif
`ifndef RANDOM
`define RANDOM $random
`endif
`ifdef RANDOMIZE_MEM_INIT
  integer initvar;
`endif
`ifndef SYNTHESIS
`ifdef FIRRTL_BEFORE_INITIAL
`FIRRTL_BEFORE_INITIAL
`endif
initial begin
  `ifdef RANDOMIZE
    `ifdef INIT_RANDOM
      `INIT_RANDOM
    `endif
    `ifndef VERILATOR
      `ifdef RANDOMIZE_DELAY
        #`RANDOMIZE_DELAY begin end
      `else
        #0.002 begin end
      `endif
    `endif
`ifdef RANDOMIZE_REG_INIT
  _RAND_0 = {1{`RANDOM}};
  register_0 = _RAND_0[31:0];
  _RAND_1 = {1{`RANDOM}};
  register_1 = _RAND_1[31:0];
  _RAND_2 = {1{`RANDOM}};
  register_2 = _RAND_2[31:0];
  _RAND_3 = {1{`RANDOM}};
  register_3 = _RAND_3[31:0];
  _RAND_4 = {1{`RANDOM}};
  register_4 = _RAND_4[31:0];
  _RAND_5 = {1{`RANDOM}};
  register_5 = _RAND_5[31:0];
  _RAND_6 = {1{`RANDOM}};
  register_6 = _RAND_6[31:0];
  _RAND_7 = {1{`RANDOM}};
  register_7 = _RAND_7[31:0];
  _RAND_8 = {1{`RANDOM}};
  register_8 = _RAND_8[31:0];
  _RAND_9 = {1{`RANDOM}};
  register_9 = _RAND_9[31:0];
  _RAND_10 = {1{`RANDOM}};
  register_10 = _RAND_10[31:0];
  _RAND_11 = {1{`RANDOM}};
  register_11 = _RAND_11[31:0];
  _RAND_12 = {1{`RANDOM}};
  register_12 = _RAND_12[31:0];
  _RAND_13 = {1{`RANDOM}};
  register_13 = _RAND_13[31:0];
  _RAND_14 = {1{`RANDOM}};
  register_14 = _RAND_14[31:0];
  _RAND_15 = {1{`RANDOM}};
  register_15 = _RAND_15[31:0];
  _RAND_16 = {1{`RANDOM}};
  result_register_0 = _RAND_16[31:0];
  _RAND_17 = {1{`RANDOM}};
  result_register_1 = _RAND_17[31:0];
  _RAND_18 = {1{`RANDOM}};
  result_register_2 = _RAND_18[31:0];
  _RAND_19 = {1{`RANDOM}};
  result_register_3 = _RAND_19[31:0];
  _RAND_20 = {1{`RANDOM}};
  result_register_4 = _RAND_20[31:0];
  _RAND_21 = {1{`RANDOM}};
  result_register_5 = _RAND_21[31:0];
  _RAND_22 = {1{`RANDOM}};
  result_register_6 = _RAND_22[31:0];
  _RAND_23 = {1{`RANDOM}};
  result_register_7 = _RAND_23[31:0];
  _RAND_24 = {1{`RANDOM}};
  result_register_8 = _RAND_24[31:0];
  _RAND_25 = {1{`RANDOM}};
  result_register_9 = _RAND_25[31:0];
  _RAND_26 = {1{`RANDOM}};
  result_register_10 = _RAND_26[31:0];
  _RAND_27 = {1{`RANDOM}};
  result_register_11 = _RAND_27[31:0];
  _RAND_28 = {1{`RANDOM}};
  result_register_12 = _RAND_28[31:0];
  _RAND_29 = {1{`RANDOM}};
  result_register_13 = _RAND_29[31:0];
  _RAND_30 = {1{`RANDOM}};
  result_register_14 = _RAND_30[31:0];
  _RAND_31 = {1{`RANDOM}};
  result_register_15 = _RAND_31[31:0];
  _RAND_32 = {1{`RANDOM}};
  result_register_16 = _RAND_32[31:0];
`endif // RANDOMIZE_REG_INIT
  `endif // RANDOMIZE
end // initial
`ifdef FIRRTL_AFTER_INITIAL
`FIRRTL_AFTER_INITIAL
`endif
`endif // SYNTHESIS
endmodule
