"""
=========
My Entity
=========

My Entity model template The System Development Kit
Used as a template for all TheSyDeKick Entities.

Current docstring documentation style is Numpy
https://numpydoc.readthedocs.io/en/latest/format.html

This text here is to remind you that documentation is important.
However, youu may find it out the even the documentation of this 
entity may be outdated and incomplete. Regardless of that, every day 
and in every way we are getting better and better :).

Initially written by Marko Kosunen, marko.kosunen@aalto.fi, 2017.

"""

import os
import sys
if not (os.path.abspath('../../thesdk') in sys.path):
    sys.path.append(os.path.abspath('../../thesdk'))

from thesdk import *
from rtl import *
import numpy as np

class exponential(rtl,thesdk):
    @property
    def _classfile(self):
        return os.path.dirname(os.path.realpath(__file__)) + "/"+__name__

    def __init__(self,*arg): 
        self.print_log(type='I', msg='Inititalizing %s' %(__name__)) 
        self.proplist = [ 'Rs' ]; # Properties that can be propagated from parent
        self.Rs =  100e6; # Sampling frequency
        self.IOS.Members['io_A']= IO() # Pointer for input data
        self.IOS.Members['io_Z']= IO() # Pointer for output data
        self.IOS.Members['control_write'] = IO() # Pointer for control IO for rtl simulations


        self.model='py'; # Can be set externally, but is not propagated
        self.par= False # By default, no parallel processing
        self.queue= [] # By default, no parallel processing

        if len(arg)>=1:
            parent=arg[0]
            self.copy_propval(parent,self.proplist)
            self.parent =parent;

        self.init()

    def init(self):
        pass #Currently nohing to add

    def main(self):
        '''Guideline. Isolate python processing to main method.
        
        To isolate the interna processing from IO connection assigments, 
        The procedure to follow is
        1) Assign input data from input to local variable
        2) Do the processing
        3) Assign local variable to output
        '''

        '''
        Idea is that:
        exp(x) is expanded to exp(k4)exp(k3)exp(k2)..., so that the value of the last exponent is approximately 1.
        k values are stored in lookup table and each of exp(k) can be represented with bit shifts and addittions, 
        i,e. we dont need multiplications. At the first iteration the result is set to 1 since the value of the last
        iteration is approximately 1.
    
        Calculation of exp(x)
        
        The algorithm operates like this:
        Iterate through the lookup table
            if:
                input angle is larger than LUT, subtract LUT from input angle and update result with bitshifts
            else:
                pass the angle and result to next stage
        '''

        # This is the lookup table for k values.
        k = [6.9314718055,5.54517177,4.158883,2.7725887,1.3862943,0.693147,0.405465108,0.22314355,
             0.117783035,0.06062462,0.03077165,0.0155041,0.0077821,0.00389864,0.00195122,
             0.000976085]
        # Lookup table for bit shifts
        bit_shift = [10,8,6,4,2,1,1,2,3,4,5,6,7,8,9,10]
        

        inval=self.IOS.Members['io_A'].Data
        # out is local variable which will be in the end passed to output
        out = len(inval)*[0]
        index = 0
        # This loop is for going through the input data
        for i in inval:
            angle_to_zero = i[0]
            result = 1
            for lut_index in range (0,len(k)):
                if (angle_to_zero > k[lut_index]):
                    #Calculate angle
                    angle_to_zero = angle_to_zero - k[lut_index]

                    #Calculate result
                    if (lut_index < 6):
                        result = (result * pow(2,bit_shift[lut_index]))
                    else:
                        result = result + (result*pow(2,-bit_shift[lut_index]))

                else:
                    # If comparison is false, we pass angle and result to next stage.
                    angle_to_zero = angle_to_zero
                    result = result

            #Update the output array
            out[index] = result
            index = index + 1

        # Here we assign the output array to output
        self.IOS.Members['io_Z'].Data=out
 
    def run(self,*arg):
        '''Guideline: Define model depencies of executions in `run` method.

        '''
        if len(arg)>0:
            self.par=True #flag for parallel processing
            self.queue=arg[0] #multiprocessing.queue as the first argument
        if self.model=='py':
            self.main()
        else:
            if self.model == 'sv':
                _=rtl_iofile(self, name='io_A', dir='in', iotype='sample', ionames=['io_A'], datatype='uint') # IO file for input A
                f=rtl_iofile(self, name='io_Z', dir='out', iotype='sample', ionames=['io_Z'], datatype='uint') # IO file for output Z
                f.verilog_io_sync='@(negedge clock)'                
                self.rtlparameters=dict([ ('g_Rs',self.Rs),]) # Defines the sample rate
                self.run_rtl()
                self.IOS.Members['io_Z'].Data=self.IOS.Members['io_Z'].Data.astype(int).reshape(-1,1)
                Out = self.IOS.Members['io_Z'].Data
                Out = Out/np.power(2,8)
                Out = np.roll(Out,-17)
                self.IOS.Members['io_Z'].Data = Out

    def define_io_conditions(self):
        '''This overloads the method called by run_rtl method. It defines the read/write conditions for the files

        '''
        # Input A is read to verilog simulation after 'initdone' is set to 1 by controller
        self.iofile_bundle.Members['io_A'].verilog_io_condition='initdone'
        # Output is read to verilog simulation when all of the outputs are valid, 
        # and after 'initdone' is set to 1 by controller
        self.iofile_bundle.Members['io_Z'].verilog_io_condition_append(cond='&& initdone')




if __name__=="__main__":
    import matplotlib.pyplot as plt
    from  exponential import *
    from  exponential.controller import controller as exponential_controller
    import pdb
    import math
    length = 2000
    rs=100e6
    max_in = 17
    indata = max_in/length * np.arange(length).reshape(-1,1)
    controller=exponential_controller()
    controller.Rs=rs
    controller.reset()
    controller.start_datafeed()

    # Models ['target' 'py','sv']
    models=['target','py','sv']
    duts=[]

    for model in models:
        d=exponential()
        duts.append(d) 
        d.model=model
        d.Rs=rs
        d.preserve_iofiles = False
        if model == 'sv':
            # For verilog model we want to bit shift the input for 28 bit, because the first
            # 28 bit are reserved for the decimals.
            d.IOS.Members['io_A'].Data = (max_in << 28)/length * np.arange(length).reshape(-1,1)
        elif model == 'target':
            d.IOS.Members['io_A'].Data = indata
            d.IOS.Members['io_Z'].Data = np.exp(indata)
        else:
            d.IOS.Members['io_A'].Data = indata

        d.IOS.Members['control_write']=controller.IOS.Members['control_write']
        d.init()
        d.run()

    for k in range(len(duts)):
        plt.title('Target, Python and Verilog',fontsize=18)
        x = max_in/length * np.arange(length).reshape(-1,1)
        plt.plot(x, duts[k].IOS.Members['io_Z'].Data, label = duts[k].model)
        plt.xlim([0,16])
        plt.ylim([0,8900000])
        plt.grid(True)
        plt.legend()

    for k in range(len(duts)):
        plt.figure()
        hfont = {'fontname':'Sans'}
        x = max_in/length * np.arange(length).reshape(-1,1)
        plt.plot(x, duts[k].IOS.Members['io_Z'].Data,linewidth=3)
        plt.xlim([0,16])
        plt.ylim([0,8900000])
        plt.grid(True)
        plt.title("Exponential function accelerator %s" %(duts[k].model), fontsize = 40)
        plt.xlabel("Input", fontsize = 24)
        plt.xticks(fontsize = 18)
        plt.ylabel("Output", fontsize = 24)
        plt.yticks(fontsize = 18)

    plt.figure()
    plt.title('Python model absolute error: abs(target-python)',fontsize=40)
    x = max_in/length * np.arange(length).reshape(-1,1)
    absolute_error = len(x)*[0]
    for i in range(0,len(x)):
        absolute_error[i] = duts[0].IOS.Members['io_Z'].Data[i]-duts[1].IOS.Members['io_Z'].Data[i]
    absolute_error = np.absolute(absolute_error)
    plt.xlim([0,16])
    plt.ylim([0,10000])
    plt.xlabel("Input", fontsize = 24)
    plt.xticks(fontsize = 18)
    plt.ylabel("Absolute error", fontsize = 24)
    plt.yticks(fontsize = 18)
    plt.grid()
    plt.plot(x, absolute_error)

    plt.figure()
    plt.title('Python model relative error: (target/python)',fontsize=40)
    x = max_in/length * np.arange(length).reshape(-1,1)
    relative_error = len(x)*[0]
    for i in range(0,len(x)):
        relative_error[i] = duts[0].IOS.Members['io_Z'].Data[i]/duts[1].IOS.Members['io_Z'].Data[i]
    plt.xlim([0,16])
    plt.ylim([0.999,1.002])
    plt.xlabel("Input", fontsize = 24)
    plt.xticks(fontsize = 18)
    plt.ylabel("Relative error", fontsize = 24)
    plt.yticks(fontsize = 18)
    plt.grid()
    plt.plot(x, relative_error)


    plt.figure()
    plt.title('Relative error: Target/Verilog',fontsize=40)
    x = max_in/length * np.arange(length).reshape(-1,1)
    relative_error = duts[0].IOS.Members['io_Z'].Data/duts[2].IOS.Members['io_Z'].Data
    plt.xlim([0,16])
    plt.ylim([0.999,1.012])
    plt.xlabel("Input", fontsize = 24)
    plt.xticks(fontsize = 18)
    plt.ylabel("Relative error", fontsize = 24)
    plt.yticks(fontsize = 18)
    plt.grid()
    plt.plot(x, relative_error)


    plt.figure()
    plt.title('Absolute error: Abs(Target - Verilog)',fontsize=40)
    x = max_in/length * np.arange(length).reshape(-1,1)
    absolute_error = np.absolute(duts[0].IOS.Members['io_Z'].Data-duts[2].IOS.Members['io_Z'].Data)
    plt.xlim([0,16])
    plt.ylim([0,15000])
    plt.xlabel("Input", fontsize = 24)
    plt.xticks(fontsize = 18)
    plt.ylabel("Relative error", fontsize = 24)
    plt.yticks(fontsize = 18)
    plt.grid()
    plt.plot(x, absolute_error)

    plt.show()
    input()
    
